﻿using System;
using System.Reflection;
using NHibernate;
using NHibernate.Cfg;
using Test.Domain;


namespace Test
{
    public sealed class SessionFactory
    {
        private static ISessionFactory iSessionFactory;


        public static ISession OpenSession
        {
            get
            {
                if (iSessionFactory == null)
                {
                    Configuration configuration = new Configuration();
                    configuration.Configure();
                    configuration.AddAssembly(typeof(Persons).Assembly);
                    iSessionFactory = configuration.BuildSessionFactory();
                }

                return iSessionFactory.OpenSession();
            }
        }
    }
}
