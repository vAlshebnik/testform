﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using NHibernate;
using OfficeOpenXml;
using Test.Domain;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void display_Click(object sender, EventArgs e)
        {
            GetPersonInfo();
        }
        private void GetPersonInfo()

        {
            try
            {
                using (ISession session = SessionFactory.OpenSession)

                {
                    IList<Persons> pInfos = session.CreateQuery("from Persons").List<Persons>();

                    dgView.DataSource = pInfos;

                    CreatePersonInfoReport(session);
                }
            }
            catch(Exception msg)
            {
                MessageBox.Show(msg.ToString());
                throw;
            }
        }

        private void CreatePersonInfoReport(ISession session)
        {
            FileInfo newFile = new FileInfo("D:\\sample1.xlsx");
            if (newFile.Exists)
            {
                newFile.Delete();
                newFile = new FileInfo("D:\\sample1.xlsx");
            }
            using (ExcelPackage package = new ExcelPackage(newFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Люди на букву А");
                worksheet.Cells["A1"].Value = "ID";
                worksheet.Cells["B1"].Value = "First Name";
                worksheet.Cells["C1"].Value = "Middle Name";
                worksheet.Cells["D1"].Value = "Last Name";
                worksheet.Cells["E1"].Value = "Job Position";
                worksheet.Cells["F1"].Value = "Mifare";

                IList<Persons> pInfos = session.CreateQuery("from Persons as person where person.firstname like 'А%'").List<Persons>();
                int row = 2;
                foreach (Persons p in pInfos)
                {
                    worksheet.Cells["A" + row].Value = p.id;
                    worksheet.Cells["B" + row].Value = p.firstname;
                    worksheet.Cells["C" + row].Value = p.middlename;
                    worksheet.Cells["D" + row].Value = p.lastname;
                    worksheet.Cells["E" + row].Value = p.jobposition;
                    worksheet.Cells["F" + row].Value = p.mifare;
                    row++;
                }

                package.Save();
            }
        }
    }
}
