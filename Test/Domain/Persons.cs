﻿namespace Test.Domain
{
    public class Persons
    {
        public virtual int id { get; set; }
        public virtual string firstname { get; set; }
        public virtual string lastname { get; set; }
        public virtual string middlename { get; set; }
        public virtual int jobposition { get; set; }
        public virtual string mifare { get; set; }

    }
}
